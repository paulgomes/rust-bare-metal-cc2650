#![feature(asm, lang_items, naked_functions)]
#![no_std]
#![no_main]

macro_rules! breakpoint {
    ($arg:expr) => (
        unsafe { asm!("BKPT $0" : : "i"($arg) : : "volatile") }
    )
}

#[naked]
pub fn main() {
    breakpoint!(1);
}

#[link_section = ".reset_handler"]
static RESET: fn() = main;

#[lang = "panic_fmt"]
fn rust_begin_panic(_msg: core::fmt::Arguments, _file: &'static str, _line: u32) -> ! {
    loop {}
}